package com.kanchanchowdhury.protectionfromdanger;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

/**
 * Created by Kanchan on 12/13/2017.
 */

public class RelativesListActivity extends AppCompatActivity implements View.OnClickListener{

    private CardView cvEmpty;
    private Button btnAddRelatives;
    private RecyclerView recyclerRelatives;
    private ArrayList<RelativeModel> relativeList;
    private RelativeListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatives_list);
        initView();
        relativeList = new ArrayList<>();
        adapter = new RelativeListAdapter(this, relativeList, cvEmpty);
        recyclerRelatives.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        relativeList = getRelativeList();
        adapter.setData(relativeList);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        getSupportActionBar().setTitle("Your Relatives");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cvEmpty = (CardView) findViewById(R.id.cv_empty);
        btnAddRelatives = (Button) findViewById(R.id.btn_add_relatives);
        recyclerRelatives = (RecyclerView) findViewById(R.id.recycler_relatives);

        recyclerRelatives.setLayoutManager(new LinearLayoutManager(this));

        btnAddRelatives.setOnClickListener(this);
    }

    private ArrayList<RelativeModel> getRelativeList() {
        ArrayList<RelativeModel> relatives = new ArrayList<>();
        SharedPreferenceHandler sharedPreferenceHandler = new SharedPreferenceHandler(this);
        int relativeCount = sharedPreferenceHandler.getRelativeCount();
        for (int i = 0; i < relativeCount; i++) {
            RelativeModel relativeModel = new RelativeModel();
            relativeModel.setName(sharedPreferenceHandler.getRelativeName(i));
            relativeModel.setMobile(sharedPreferenceHandler.getRelativeMobile(i));
            relatives.add(relativeModel);
        }
        return relatives;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_relatives:
                startActivity(new Intent(RelativesListActivity.this, AddRelativesActivity.class));
                break;
        }
    }

}
