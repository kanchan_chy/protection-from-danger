package com.kanchanchowdhury.protectionfromdanger;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by kanchan on 12/12/17.
 */

public class Utility {

    public void sendSms(Context context, String text) {
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS)  == PackageManager.PERMISSION_GRANTED) {
            SmsManager manager=SmsManager.getDefault();
            ArrayList<String> relativeNumbers = getRelativeNumbers(context);
            for (int i = 0; i < relativeNumbers.size(); i++) {
                manager.sendTextMessage(relativeNumbers.get(i), null, text, null, null);
                Log.e("sms_sent", relativeNumbers.get(i));
            }
        }
    }

    private ArrayList<String> getRelativeNumbers(Context context) {
        SharedPreferenceHandler sharedPreferenceHandler = new SharedPreferenceHandler(context);
        int count = sharedPreferenceHandler.getRelativeCount();
        ArrayList<String> numbers = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            numbers.add(sharedPreferenceHandler.getRelativeMobile(i));
        }
        return numbers;
    }

}
