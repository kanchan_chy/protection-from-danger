package com.kanchanchowdhury.protectionfromdanger;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Kanchan on 12/13/2017.
 */

public class RelativeListAdapter extends RecyclerView.Adapter<RelativeListAdapter.RelativeViewHolder> {

    private Context mContext;
    private ArrayList<RelativeModel> mRelativeList;
    private CardView mCvEmpty;

    public RelativeListAdapter(Context context, ArrayList<RelativeModel> relativeList, CardView cvEmpty) {
        mContext = context;
        mRelativeList = relativeList;
        mCvEmpty = cvEmpty;
    }

    @Override
    public RelativeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new RelativeViewHolder(inflater.inflate(R.layout.row_relative_list, parent, false));
    }

    @Override
    public void onBindViewHolder(RelativeViewHolder holder, int position) {
        holder.txtName.setText(mRelativeList.get(position).getName());
        holder.txtMobile.setText(mRelativeList.get(position).getMobile());
    }

    @Override
    public int getItemCount() {
        return mRelativeList.size();
    }

    public void setData(ArrayList<RelativeModel> relativeList) {
        mRelativeList = relativeList;
        notifyDataSetChanged();
        if(mRelativeList.size() == 0) {
            mCvEmpty.setVisibility(View.VISIBLE);
        } else {
            mCvEmpty.setVisibility(View.GONE);
        }
    }

    private void showDeleteDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Alert");
        builder.setMessage("Are you sure you want to delete this relative?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new SharedPreferenceHandler(mContext).deleteRelative(position, mRelativeList);
                mRelativeList.remove(position);
                notifyItemRemoved(position);
                if(mRelativeList.size() == 0) {
                    mCvEmpty.setVisibility(View.VISIBLE);
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public class RelativeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView txtName;
        TextView txtMobile;
        ImageButton imgBtnDelete;

        public RelativeViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txt_name);
            txtMobile = (TextView) itemView.findViewById(R.id.txt_mobile);
            imgBtnDelete = (ImageButton) itemView.findViewById(R.id.img_btn_delete);

            imgBtnDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.img_btn_delete:
                  showDeleteDialog(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }

    }

}
