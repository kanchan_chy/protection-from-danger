package com.kanchanchowdhury.protectionfromdanger;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Kanchan on 12/13/2017.
 */

public class AddRelativesActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnAdd;
    EditText edtName;
    EditText edtMobile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_relatives);
        initView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        getSupportActionBar().setTitle("Add New Relative");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnAdd = (Button) findViewById(R.id.btn_add);
        edtName = (EditText) findViewById(R.id.edt_name);
        edtMobile = (EditText) findViewById(R.id.edt_mobile);
        btnAdd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                if(!TextUtils.isEmpty(edtName.getText().toString().trim()) && !TextUtils.isEmpty(edtMobile.getText().toString().trim())) {
                    new SharedPreferenceHandler(this).addRelative(edtName.getText().toString().trim(), edtMobile.getText().toString().trim());
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Please fill up all options", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

}
