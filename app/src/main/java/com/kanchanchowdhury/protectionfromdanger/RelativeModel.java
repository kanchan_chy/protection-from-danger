package com.kanchanchowdhury.protectionfromdanger;

/**
 * Created by Kanchan on 12/13/2017.
 */

public class RelativeModel {

    private String name;
    private String mobile;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

}
