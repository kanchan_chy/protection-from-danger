package com.kanchanchowdhury.protectionfromdanger;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new SplashTask().execute();
    }

    class SplashTask extends AsyncTask<String, String, String>
    {

        @Override
        protected String doInBackground(String... arg0) {
            // TODO Auto-generated method stub
            try
            {
                Thread.sleep(500);
            }
            catch(Exception e)
            {

            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            Intent in=new Intent(MainActivity.this,OptionsActivity.class);
            //	stopStatus=true;
            startActivity(in);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            finish();
        }

    }

}
