package com.kanchanchowdhury.protectionfromdanger;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by Kanchan on 12/20/2017.
 */

public class InstructionsActivity extends AppCompatActivity {

    private TextView txtInstructionDetails;

    private String instruction = "This application is designed to help the you in any kind of danger where help of your relatives is a very important factor. " +
            "Especially, women could use this app to prevent sexual harassment. This app will help you by sending your location and information about your danger to your relatives via SMS. " +
            "You should add your relatives\' mobile number in the Relatives section of this application. SMS will be sent to the numbers added in the Relatives section." +
            "\n\nIf you make the shaking service turned on, a service will be activated and will look for occurence of shaking. If shaking is detected above a certain level, this app will inform your relatives about your danger. It will also send your location at the time of shaking." +
            "So, if you face any danger, including sexual harassment, just shake your device very highly." +
            "\n\nAgain, if you don't want the shaking service, you could make it turned on. In that case, no service will be activated to monitor shaking. You could also send SMS to your relatives by manaually pressing the Send SMS button.";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instructions);

        getSupportActionBar().setTitle("Instructions");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtInstructionDetails = (TextView) findViewById(R.id.txt_instruction_details);
        txtInstructionDetails.setText(instruction);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
