package com.kanchanchowdhury.protectionfromdanger;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.List;
import java.util.Locale;

/**
 * Created by kanchan on 12/12/17.
 */

public class GpsLocationListener implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Context mContext;
    private GoogleApiClient mGoogleApiClient;

    public GpsLocationListener(Context context) {
        mContext = context;
        initGoogleApiClient();
    }

    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void connectGoogleApiClient() {
        if(mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else {
            Log.e("something_wrong", "Google API Client not initialized");
        }
    }

    private void getAddress(double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(mContext, Locale.getDefault());

            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            if(addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                String fullAddress = address + ", " + city;
                if(!TextUtils.isEmpty(state)) {
                    fullAddress += ", " + state;
                }
                fullAddress += ", " + country;

                String text = "Kanchan is in Danger. Please help him/her. His current location is given below:\nAddress: " + fullAddress;
                if(!TextUtils.isEmpty(postalCode)) {
                    text += "\nPostal Code: " + postalCode;
                }
                if(!TextUtils.isEmpty(knownName)) {
                    text += "\nKnown Name: " + knownName;
                }

                Log.e("address", text);
                new Utility().sendSms(mContext, text);
            }
        } catch (Exception ex) {

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("connected", "yes");
        if(ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)  == PackageManager.PERMISSION_GRANTED) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if(location != null) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                Log.e("Location", "Lat: " + latitude + ", lon: " + longitude);
                getAddress(latitude, longitude);
                mGoogleApiClient.disconnect();
            } else {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("connection_suspended", "yes");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("connection_failed", "yes");
        mGoogleApiClient.connect();
    }
}
