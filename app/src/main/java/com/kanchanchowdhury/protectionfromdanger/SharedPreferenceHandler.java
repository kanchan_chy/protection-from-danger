package com.kanchanchowdhury.protectionfromdanger;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

/**
 * Created by kanchan on 12/11/17.
 */

public class SharedPreferenceHandler {

    private SharedPreferences shakePreferences;
    private SharedPreferences.Editor shakeEditor;
    private SharedPreferences relativePreferences;
    private SharedPreferences.Editor relativeEditor;
    private Context mContext;

    public SharedPreferenceHandler(Context context) {
        mContext = context;

        shakePreferences = mContext.getSharedPreferences("shake_service", 0);
        shakeEditor = shakePreferences.edit();

        relativePreferences = mContext.getSharedPreferences("relatives_pref", 0);
        relativeEditor = relativePreferences.edit();
    }

    public void setShakeServiceState(boolean isOn) {
        shakeEditor.putBoolean("is_shaking_on", isOn);
        shakeEditor.commit();
    }

    public boolean isShakeServiceOn() {
        return shakePreferences.getBoolean("is_shaking_on", false);
    }

    public void addRelative(String name, String mobile) {
        int count = getRelativeCount();
        relativeEditor.putString("name_" + count, name);
        relativeEditor.putString("mobile_" + count, mobile);
        count++;
        relativeEditor.putInt("relative_count", count);
        relativeEditor.commit();
    }

    public void deleteRelative(int position, ArrayList<RelativeModel> relativeList) {
        relativeEditor.clear();
        relativeEditor.commit();
        for(int i = 0; i < relativeList.size(); i++) {
            if(position != i) {
                addRelative(relativeList.get(i).getName(), relativeList.get(i).getMobile());
            }
        }
    }

    public int getRelativeCount() {
        return relativePreferences.getInt("relative_count", 0);
    }

    public String getRelativeName(int pos) {
        return relativePreferences.getString("name_" + pos, "");
    }

    public String getRelativeMobile(int pos) {
        return relativePreferences.getString("mobile_" + pos, "");
    }

}
