package com.kanchanchowdhury.protectionfromdanger;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Kanchan on 11/28/2017.
 */

public class OptionsActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener{

    private TextView txtSendSms;
    private SwitchCompat switchShake;
    private TextView txtRelatives;
    private TextView txtInstructions;
    private SharedPreferenceHandler sharedPreferenceHandler;
    private PermissionHandler permission;
    private boolean sendSms;

    private static final int REQUEST_PERMISSION_SEND_SMS = 1;
    private static final int REQUEST_PERMISSION_ACCESS_FINE_LOCATION = 2;
    private static final int REQUEST_PERMISSION_SEND_SMS_AND_ACCESS_FINE_LOCATION = 3;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        initView();
        setUiClickHandler();
        initData();
    }

    private void initView() {
        txtSendSms = (TextView) findViewById(R.id.txt_send_sms);
        switchShake = (SwitchCompat) findViewById(R.id.switch_shake);
        txtRelatives = (TextView) findViewById(R.id.txt_relatives);
        txtInstructions = (TextView) findViewById(R.id.txt_instructions);
    }

    private void setUiClickHandler() {
        txtSendSms.setOnClickListener(this);
        txtRelatives.setOnClickListener(this);
        txtInstructions.setOnClickListener(this);
        switchShake.setOnCheckedChangeListener(this);
    }

    private void initData() {
        sendSms = false;
        permission = new PermissionHandler(this);
        sharedPreferenceHandler = new SharedPreferenceHandler(this);
        switchShake.setChecked(sharedPreferenceHandler.isShakeServiceOn());

        checkPermissions();
    }

    private void checkPermissions() {
        if(!permission.hasSendSmsPermission() && !permission.hasAccessFineLocationPermission()) {
            requestBothSendSmsAndAccessLocationPermission();
        } else if(!permission.hasSendSmsPermission()) {
            requestSendSmsPermission();
        } else if(!permission.hasAccessFineLocationPermission()) {
            requestAccessLocationPermission();
        } else {
            if(sendSms) {
                getLocationAndsendSMS();
                sendSms = false;
            }
        }
    }

    private void getLocationAndsendSMS() {
        new GpsLocationListener(this).connectGoogleApiClient();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_SEND_SMS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(sendSms) {
                        getLocationAndsendSMS();
                    }
                } else {
                    Toast.makeText(this, "This app was denied access to sending and viewing SMS. Please grant permission for proper functioning.", Toast.LENGTH_LONG).show();
                }
                sendSms = false;
                break;
            case REQUEST_PERMISSION_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(sendSms) {
                        getLocationAndsendSMS();
                    }
                } else {
                    Toast.makeText(this, "This app was denied access to GPS location. Please grant permission for proper functioning.", Toast.LENGTH_LONG).show();
                }
                sendSms = false;
                break;
            case REQUEST_PERMISSION_SEND_SMS_AND_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(sendSms) {
                        getLocationAndsendSMS();
                    }
                } else {
                    Toast.makeText(this, "This app was denied access to GPS location and sending and viewing SMS. Please grant permission for proper functioning.", Toast.LENGTH_LONG).show();
                }
                sendSms = false;
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch(buttonView.getId()) {
            case R.id.switch_shake:
                if(isChecked) {
                    if(!sharedPreferenceHandler.isShakeServiceOn()) {
                        startService(new Intent(OptionsActivity.this, ShakingService.class));
                        sharedPreferenceHandler.setShakeServiceState(true);
                        Snackbar.make(findViewById(android.R.id.content), "Please make sure that your app has permission for both sending SMS and accessing GPS location", Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    if(sharedPreferenceHandler.isShakeServiceOn()) {
                        stopService(new Intent(OptionsActivity.this, ShakingService.class));
                        sharedPreferenceHandler.setShakeServiceState(false);
                    }
                }
                break;
        }
    }

    private void requestSendSmsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {
            permission.showPermissionSettingsSnackbar("You need to authorize this app to send and view SMS from your Settings");
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, REQUEST_PERMISSION_SEND_SMS);
        }
    }

    private void requestAccessLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            permission.showPermissionSettingsSnackbar("You need to authorize this app to access GPS location from your Settings");
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_ACCESS_FINE_LOCATION);
        }
    }

    private void requestBothSendSmsAndAccessLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)
                && ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            permission.showPermissionSettingsSnackbar("You need to authorize this app to send and view SMS and access GPS location from your Settings");
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {
            permission.showPermissionSettingsSnackbar("You need to authorize this app to send and view SMS from your Settings");
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            permission.showPermissionSettingsSnackbar("You need to authorize this app to access GPS location from your Settings");
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS, Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_SEND_SMS_AND_ACCESS_FINE_LOCATION);
        }
    }

    private void showSMSConfirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setMessage("Are you sure you want to send SMS of danger to your relatives?");
        builder.setPositiveButton("Send SMS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int MyVersion = Build.VERSION.SDK_INT;
                if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    sendSms = true;
                    checkPermissions();
                } else {
                    getLocationAndsendSMS();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_send_sms:
                showSMSConfirmDialog();
                break;
            case R.id.txt_relatives:
                startActivity(new Intent(OptionsActivity.this, RelativesListActivity.class));
                break;
            case R.id.txt_instructions:
                startActivity(new Intent(OptionsActivity.this, InstructionsActivity.class));
                break;
        }
    }
}
